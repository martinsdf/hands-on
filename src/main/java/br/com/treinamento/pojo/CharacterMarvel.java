package br.com.treinamento.pojo;

public class CharacterMarvel {
	
	private int id;
	private String name ;
	private String description;
	private String modified; 
	private String resourceURI; 
//	private String urls ;
//	private String thumbnail ;
//	private String comics ;
//	private String stories ;
//	private String events;
//	private String series;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public String getResourceURI() {
		return resourceURI;
	}
	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}
	
}
