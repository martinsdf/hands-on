package br.com.treinamento.dojo.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import br.com.treinamento.pojo.CharacterDataWrapper;

@RestController
public class MarvelController {
	
	private static String BASE_URL = "http://gateway.marvel.com";
	@RequestMapping(value = "/listarPersonagens", method = RequestMethod.GET)
	public ResponseEntity<String> listarPersonagens() {
		 RestTemplate restTemplate = new RestTemplate();
		 String result = restTemplate.getForObject(getUrl(), String.class);
		 Gson gson = new Gson();
		 CharacterDataWrapper json = gson.fromJson(result, CharacterDataWrapper.class);
		 System.out.println(result);

		return new ResponseEntity<String>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/recuperarPorId/{characterId}",  method = RequestMethod.GET)
	public ResponseEntity<String> recuperarPorId(@PathVariable(value="characterId") String characterId) {
		 RestTemplate restTemplate = new RestTemplate();
		 String result = restTemplate.getForObject(getUrl(characterId), String.class);
		 Gson gson = new Gson();
		 CharacterDataWrapper json = gson.fromJson(result, CharacterDataWrapper.class);
		 System.out.println(result);

		return new ResponseEntity<String>(result, HttpStatus.OK);
	}

	private String getUrl() {
		StringBuilder sb = new StringBuilder(BASE_URL);
		sb.append("/v1/public/characters?");
		Long data = new Date().getTime();
		String pvKey = "bf7939b23d78e4eaa6e39c76ed61638907c9224b";
		String publicKey = "9bb83071b7a6e858d8aa4e50296aebf2"; 
		String hash = generateHash(data + pvKey + publicKey);
		sb.append("ts=").append(data).append("&hash=").append(hash);
		sb.append("&apikey=").append(publicKey);
		return sb.toString();
	}
	private String getUrl(String id) {
		StringBuilder sb = new StringBuilder(BASE_URL);
		sb.append("/v1/public/characters/"+id+"?");
		Long data = new Date().getTime();
		String pvKey = "bf7939b23d78e4eaa6e39c76ed61638907c9224b";
		String publicKey = "9bb83071b7a6e858d8aa4e50296aebf2"; 
		String hash = generateHash(data + pvKey + publicKey);
		sb.append("ts=").append(data).append("&hash=").append(hash);
		sb.append("&apikey=").append(publicKey);
		return sb.toString();
	}

	private  String generateHash(String string) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		md.update(string.getBytes());
		byte[] bytes = md.digest();

		StringBuilder s = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			int parteAlta = ((bytes[i] >> 4) & 0xf) << 4;
			int parteBaixa = bytes[i] & 0xf;
			if (parteAlta == 0) {
				s.append('0');
			}
			s.append(Integer.toHexString(parteAlta | parteBaixa));
		}
		return s.toString();
	}
}
